purchase box from online.net
if you want to connect the box via vpn to another machine, be sure
that it's RPN enabled.

In the server setup, choose
1. install over KVM
1. virtualization
1. proxmox latest version
1. run the scripts to set up the machine and port forwarding
1. create vmbr1 bridge
1. Create a vm container
    1. hostname: vpn
    1. Disk: 16GB
    1. 2 cores
    1. 1GB memory
    1. ip: 172.50.10.2/32
    1. gateway: 172.50.10.1
    1. vmbr1 bridge
1. Add `lxc.mount.entry = /dev/net/tun dev/net/tun none bind,create=file` into `/etc/pve/lxc/100.conf`
1. reboot root machine
1. Start the vpn vm
1. Setup vpn machine
    1. ssh and do the normal update / upgrad and install vim
    1. make sure the root user on the root machine has it's
     ssh public key in vpn authorized_keys file
    1. run the stuff in install-openvpn-server.sh
    1. set the admin login with `passwd openvpn`
1. configure openvpn via admin interface
    1. Log in by using the username `openvpn` and the pass you just set by going to `https://<ip>:943/admin/`
    1. server network settings
        1. set hostname
        1. TCP for protocol
        1. set port to 1443
    1. vpn settings
        1. private subnet text input box -> 172.50.10.0/24
        1. Should client Internet traffic be routed through the VPN? -> NO
1. Add user
    1. add a user to vpn box (ssh sam.shax.site -> ssh 172.50.10.2)
    1. run ansibles to the rest of the boxes
1. send user instructions
    1. send them username / password
    1. go to https://feta.tenfive.io:943
    1. if you get a ssl certificate error, just choose advanced and accept. we really should fix this, don't make it a habbit.
    1. enter in username / pass
    1. download and install client
    1. open the client and select "connect" (from your app tray up top) and enter the same username / pass again
    1. now you can ssh with me@172.50.10.<sam-fill-in>


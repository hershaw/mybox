# add this to your bashrc

export PYTHONPATH=$PYTHONPATH:.
alias python="python3"

# in order to set utf-8 support, install the english language packages
# sudo apt-get -y install `check-language-support -l en_US`
# this will allow you to put the following in the .bashrc

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

export WORKON_HOME=~/.virtualenvs
alias sb="source ~/.bashrc"
function workon() {
    source $WORKON_HOME/$1/bin/activate;
}
export -f workon

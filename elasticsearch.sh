#!/bin/bash

# Installation commands and instructions for ubuntu 16.04

# references: 
# https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html
# https://www.elastic.co/guide/en/kibana/current/deb.html

sudo apt-get update
sudo apt-get install htop vim buildessential

sudo locale-gen en_US.UTF-8  

echo "export LC_CTYPE=en_US.UTF-8" >> /etc/environment
echo "export LC_ALL=en_US.UTF-8" >> /etc/environment


sudo apt-get install default-jre
sudo apt-get install default-jdk

sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update

sudo apt-get install oracle-java8-installer

echo "JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/" >> /etc/environment

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install apt-transport-https
sudo apt-get update && sudo apt-get install elasticsearch

sudo -i service elasticsearch start

sudo apt-get install kibana
sudo update-rc.d kibana defaults 95 10


echo "don't forget to fix the host in the file here: /etc/kibana/kibana.yml"
echo "don't forget to fix the host here: /etc/elasticsearch/elasticsearch.yml"

echo "hit enter when you are done"
read input

sudo -i service elasticsearch restart
sudo -i service kibana restart

echo "now go through the x-pack install instructions here: https://www.elastic.co/downloads/x-pack"


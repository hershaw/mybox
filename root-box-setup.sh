#!/bin/bash

# this should be run as root
apt-get update
apt-get upgrade

apt-get install tmux vim htop iptables-persistent

cd /etc/iptables
iptables-restore rules.v4

iptables -vL -t nat

ifconfig

reboot

iptables -vL -t nat

pveam update
# You can get a list of available distros here: http://download.proxmox.com/images/system/
# This one we'll use for the vpn
pveam download local ubuntu-16.04-standard_16.04.5-1_amd64.tar.gz
# This one we'll use for the actual work boxes
pveam download local ubuntu-19.04-standard_19.04-1_amd64.tar.gz

# Add the following to the root crontab
@reboot echo 1 > /proc/sys/net/ipv4/ip_forward
